# WCD Upload Acivity Logger #


### A Laravel based app that keeps track on your what.cd upload activity ###

## What does this app do ##
It logs your what.cd upload activity. Shows the current upload (in Gb), your hourly growth, your daily growth, your ratio and number of seeded torrents each hour. It uses [Gazelle JSON API](https://github.com/WhatCD/Gazelle/wiki/JSON-API-Documentation) to get this information, and it stores it to a local database of choise.

## Requirements ##
* It should run on any OS, but this setup is made for linux/unix.
* Php `composer`. If you still don't have it, get it [here](https://getcomposer.org).
* The [requirements of Laravel 5.2](https://laravel.com/docs/5.2)
* Some database supported by Laravel 5.2 (Initailly it is LiteSQL)
* Cron (optional, but still essential for automatic work)
## Setup ##
* Clone the repository
* Go to www/ folder, do a `$ php composer install`.
* Set up the data in the .env file, setup what.cd credentials and a database of choise, initally LiteSQL, which should be ready to use out of the box. You can get your What.CD user id from the URL query string when you go to your profile on the website.
* Run the migration `$ php artisan migrate` . This will set up a table for the entries in the database.
* (**optional**) Add a cronjob `* * * * * /(path-to-php)/php /path-to-your-installation/www/artisan schedule:run >> /dev/null 2>&1` so each hour your app will make a chekcup on what.cd to extract your data. By default checkups are made every 10th minute of the hour.
* (**optional**) If you run the app localy, you can set a host (for example wcd-logger.lcl) in /etc/hosts, and to start an php app server: `$ artisan serve --host wcd-logger.lcl `. So your app-root will be wcd-logger.lcl.
* First you can force a ckeckup by addressing http://(app-root)/force-fetch to get a single data row.
* Open a browser and point it to the http://(app-root). You should see this singe row of data showing your current upload status.
* If you have set a cronjob, after about a day of work, you should see something like this:

![WCD Upload Logger](http://s31.postimg.org/mhdl34o0b/screen1.png)

Please, keep in mind that no security has been implemented in this app. So if you run it on some publicly accessible host, you should take care of this.