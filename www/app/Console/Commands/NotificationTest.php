<?php

namespace WCDLogger\Console\Commands;

use Illuminate\Console\Command;

class NotificationTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification-test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the notification';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $upl = 5.43;
        $score = floor($upl / 100);
        for ($k = 0 ; $k < $score; $k++) {
            @$icons .= ':mushroom: ';
        }
        if ($upl < 100) {
            @$icons = ':turtle:';
        }
        if ($upl < 50) {
            @$icons = ':snail:';
        }
        if ($upl < 10) {
            @$icons = ':beetle:';
        }
        if ($upl == 0) {
            @$icons = ':small_red_triangle_down:';
        }

        $client = new \Maknz\Slack\Client('https://hooks.slack.com/services/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');
        $client
        // ->attach([
        // // 'fallback' => 'Current server stats',
        // // 'text' => 'Current server stats',
        // 'color' => 'danger',
        // 'fields' => [
        //         [
        //             'title' => 'Last hour, '.$entry->time(),
        //             'value' => "{$entry->uploadDiffInMb} Mb",
        //             'short' => true
        //         ],
        //         [
        //             'title' => 'Total',
        //             'value' => number_format($entry->uploadedInGb, 2). " Mb",
        //             'short' => true
        //         ]
        //     ]
        // ])
        ->send("Upload report for the last hour (21-10-2016 10:10:05)
> S: {$icons}   U: *560 Mb*       R: 5.51
            ", [
            'username' => 'The Logger',
            'channel' => '#wcd_upload',
            'link_names' => true,
            "mrkdwn" => true
        ])
        ;

        $this->comment('Sent!');
    }
}
