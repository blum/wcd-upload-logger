<?php

namespace WCDLogger\Services;

use Mail;
use Illuminate\Http\Request;
use WCDLogger\Entry;
use Carbon\Carbon;

class WCDService
{
    private $client;
    private $jar;

    public function __construct()
    {
        $this->jar = new \GuzzleHttp\Cookie\CookieJar;
        $this->client = new \GuzzleHttp\Client(['cookies' => $this->jar, 'base_uri' => 'https://what.cd/']);

        $r = $this->client->request('POST', 'login.php', [
            'cookies' => $this->jar,
            'form_params'    => [
              'username'   => env('WCD_USERNAME'),
              'password'   => env('WCD_PASSWORD'),
              'keeplogged' => 1,
              'login'      => 'Login',
            ]
        ]);
    }

    public function getLogin()
    {
    }

    public function saveProfile()
    {
        $r = $this->client->request('GET', 'ajax.php', [
            'cookies' => $this->jar,
            'query' => [
                'action' => 'user',
                'id' => env('WCD_ID')
            ]
        ]);

        $a = json_decode($r->getBody()->getContents());

        if (@$a->status != 'success' ) {
            throw new \Exception("Can not log in!", 407474766);
        }

        $response = $a->response;

        $entry = Entry::create([
            'downloaded' => $response->stats->downloaded,
            'uploaded' => $response->stats->uploaded,
            'ratio' => $response->stats->ratio,
            'seeding' => $response->community->seeding,
        ]);

        // Get the news
        $r = $this->client->request('GET', 'ajax.php', [
            'cookies' => $this->jar,
            'query' => [
                'action' => 'announcements'
            ]
        ]);

        $a = json_decode($r->getBody()->getContents());
        $response = $a->response;

        if ($response->announcements[0]->newsId != 349) {
            Mail::raw('What.cd new announcement!', function ($m) {
                $m->from('info@wcd-logger.i-creativ.net', 'WCD Logger');

                $m->to('blum@i-creativ.net', "Ivan Yonov")->subject('Some new announcement on what.cd / '.time());
            });
        }
        // -- Get the news ----

        $this->logout();

        $entries = Entry
            ::orderBy('created_at', 'desc')
            ->take(2)
            ->get()
        ;

        $days = [];

        @$days[(new Carbon())->format('Ydm').'_last'] = true;
        $entries->transform(function ($entry) use ($entries, & $days, & $date) {
            $prev = $entries->find($entry->id - 1);
            $entry->uploadDiff = $entry->uploaded - @$prev->uploaded;

            $date = $entry->created_at->format('Ydm');
            @$days[$entry->created_at->format('Ydm')] += $entry->uploadDiff/pow(1024,floor(2));

            return $entry;
        });

        @$days[$date.'_first'] = true;

        $entry = $entries->first();

        $uploadDiffInMb = $entry->uploadDiffInMb;

        $score = floor($uploadDiffInMb / 100);
        for ($k = 0 ; $k < $score; $k++) {
            @$icons .= ':mushroom: ';
        }
        if ($uploadDiffInMb < 100) {
            @$icons = ':turtle:';
        }
        if ($uploadDiffInMb < 50) {
            @$icons = ':snail:';
        }
        if ($uploadDiffInMb < 10) {
            @$icons = ':beetle:';
        }
        if ($uploadDiffInMb == 0) {
            @$icons = ':small_red_triangle_down:';
        }

        $client = new \Maknz\Slack\Client('https://hooks.slack.com/services/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');
        $client
        // ->attach([
        // // 'fallback' => 'Current server stats',
        // // 'text' => 'Current server stats',
        // 'color' => 'danger',
        // 'fields' => [
        //         [
        //             'title' => 'Last hour, '.$entry->time(),
        //             'value' => "{$uploadDiffInMb} Mb",
        //             'short' => true
        //         ],
        //         [
        //             'title' => 'Total',
        //             'value' => number_format($entry->uploadedInGb, 2). " Mb",
        //             'short' => true
        //         ]
        //     ]
        // ])
        ->send("Upload report for the last hour ({$entry->time()})
> S: {$icons}   U: *{$uploadDiffInMb} Mb*       R: {$entry->ratio}
            ", [
            'username' => 'The Logger',
            'channel' => '#wcd_upload',
            'link_names' => true
        ])
        ;

        return $entry;
    }

    public function logout()
    {
        $r = $this->client->request('GET', 'ajax.php', [
            'cookies' => $this->jar,
            'query' => [
                'action' => 'index'
            ]
        ]);

        $a = json_decode($r->getBody()->getContents());

        $response = $a->response;
        $authkey = $response->authkey;

        $r = $this->client->request('GET', 'logout.php', [
            'cookies' => $this->jar,
            'query' => [
                'auth' => $authkey
            ]
        ]);

        $a = json_decode($r->getBody()->getContents());
    }
}
