<?php

namespace WCDLogger\Http\Controllers;

use WCDLogger\Entry;
use WCDLogger\Http\Controllers\Controller;
use WCDLogger\Http\Requests;
use WCDLogger\Services\WCDService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function __construct()
    {
    }

    public function getIndex()
    {
        $entries = Entry
            ::orderBy('created_at', 'desc')
            ->take(200)
            ->get()
        ;

        $days = [];

        @$days[(new Carbon())->format('Ydm').'_last'] = true;
        $entries->transform(function ($entry) use ($entries, & $days, & $date) {
            $prev = $entries->find($entry->id - 1);
            $entry->uploadDiff = $entry->uploaded - @$prev->uploaded;

            $date = $entry->created_at->format('Ydm');
            @$days[$entry->created_at->format('Ydm')] += $entry->uploadDiff/pow(1024,floor(2));

            return $entry;
        });

        // dd($date);
        @$days[$date.'_first'] = true;

        return view('index')
            ->with('entries', $entries)
            ->with('days', $days)
        ;
    }

    public function getForceFetch(WCDService $wcd)
    {
        try {
            $wcd->saveProfile();
        } catch (\Exception $e) {
            echo ($e->getMessage());
            die();
        }

        echo "Successful checkup. Go to <a href='/'>entries list</a> and see the result.";
    }
}
