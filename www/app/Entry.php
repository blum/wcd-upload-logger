<?php

namespace WCDLogger;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $guarded = [];

    public function getUploadedInGbAttribute()
    {
        return round($this->uploaded/pow(1024,floor(3)), 5);
    }

    public function getUploadDiffInMbAttribute($value='')
    {
        return round($this->uploadDiff/pow(1024,floor(2)), 2);
    }

    public function time()
    {
        return $this->created_at->format('d-m-Y H:i:s');
    }
}
