@extends('layout.master')

@section('content')

<div class="page-header">
  <h1>WCD Upload Logger</h1>
</div>

@if($entries->isEmpty())
    <p>There are no entries yet. Go make a <a href="/force-fetch">force checkup</a> and (if you still haven't) put a cronjob to push a checkups every hour.</p>
@endif

<ul>
    @foreach($entries as $entry)
    <li class="entries">


        <span class="hours hour_{{ $entry->created_at->format("H") }}">{{ $entry->time() }}</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        <span title="Ratio: {{ $entry->ratio }}">R: {{ number_format($entry->ratio, 2) }} &nbsp;&nbsp;&nbsp;</span>

        <span title="Total seeding">S: {{ $entry->seeding }} &nbsp;&nbsp;&nbsp;</span>

        <span title="Total upload">U: {{ number_format($entry->uploadedInGb, 2) }} Gb &nbsp;&nbsp;&nbsp;</span>

        (<u>+{{$entry->uploadDiffInMb}} Mb</u>)

        @if($days[$entry->created_at->format('Ydm')])
            - (
            @if (@$days[$entry->created_at->format('Ydm').'_first']) Until this day
            @elseif(@$days[$entry->created_at->format('Ydm').'_last']) Today
            @else This day
            @endif

            : <strong>{{round($days[$entry->created_at->format('Ydm')], 2)}} Mb</strong>)</li>
        @endif

        <?php $days[$entry->created_at->format('Ydm')] = false; ?>
    @endforeach
</ul>


@endsection